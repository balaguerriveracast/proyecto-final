//Crear un Sistema que solicite el monto, cantidad de cuotas o (plazo) en meses, porcentaje de interes anual de un préstamo. Calcule la cuota mensual y muestre la tabla amortizada de los meses.

using System;

class Sistema 
{
  public float cuota, interes, capital, monto, tasaAnual, tmensual;
  public int f, plazo;

  public void Mostrar()
  {
    Console.WriteLine("Bienvenido");
    Console.Write("1.Digite el monto del prestammos: ");
    monto= float.Parse(Console.ReadLine());
    Console.Write("2.Digite el interes anual: ");
    tasaAnual= float.Parse(Console.ReadLine());
    Console.Write("3.Digite el plazo en meses: ");
    plazo= int.Parse(Console.ReadLine());

    //Calcular
    tmensual= (tasaAnual/100)/12; //interes mensual
    cuota= tmensual + 1;
    cuota= (float)Math.Pow(cuota, plazo);
    cuota-=1;
    cuota= (tmensual / cuota); //+pago;
    cuota= tmensual + cuota;
    cuota = monto * cuota;

  }
  
  //public void Calcular();


  public void Imprimir()
  {
    for (int i = 1;i <= plazo; i++)
    {
      f+=1;
      interes= tmensual*monto;
      capital= cuota-interes;
      monto-=capital;
      Console.WriteLine("Pagos mensuales: {0} ",f);
      Console.WriteLine("~Cuota: {0}   ~Capital: {1}   ~Interes: {2}   ~Balance: {3}" , cuota, capital, interes, monto);
      Console.WriteLine();
    }
    Console.ReadKey();
  }

  public static void Main (string[] args) 
  {
    Sistema s= new Sistema();
    s.Mostrar();
    //s.Calcular();
    s.Imprimir();
  }
}
